<?php
namespace Absolutemg\Calendar;

use Carbon\Carbon;
use DateTimeZone;
use Exception;
use Twig_Environment;
use Twig_Loader_Array;

class Calendar
{
    /**
     * The 4 digit year for the calendar.
     * @var int
     */
    private $_year;

    /**
     * The month for the calendar, either 1 or 2 digits.
     * @var int
     */
    private $_month;

    /**
     * The timezone to use for the event dates.
     * @var DateTimeZone
     */
    private $_timezone;

    /**
     * The first day of the month to display, expressed as a Carbon instance.
     * @var Carbon
     */
    private $_monthStartDate;

    /**
     * The last day of the month to display, expressed as a Carbon instance.
     * @var Carbon
     */
    private $_monthEndDate;

    /**
     * An array containing the days of the week.
     * @var array
     */
    private $_weekdays;

    /**
     * The html for the calendar template.
     * @var string
     */
    private $_templateHtml;

    /**
     * The base url for the event overflow link.
     * @var string
     */
    private $_overflowBaseUrl;

    /**
     * The base url for the month navigation links.
     * @var string
     */
    private $_monthNavBaseUrl;

    /**
     * A flag indicating if the URLs use query string or not. Defaults to true.
     * @var boolean
     */
    private $_useQueryStrings = true;

    /**
     * The number of events to display for a single day. Default value is -1, which indicates no limit.
     * @var int
     */
    private $_dailyEventLimit = -1;

    /**
     * An array containing the event data. Defaults to an empty array.
     * @var array
     */
    private $_events = [];

    /**
     * Instantiates the class.
     * @param int $year The 4 digit year. Defaults to current year.
     * @param int $month The month. Defaults to current month.
     * @param DateTimeZone|null $timezone The timezone object. Defaults to UTC.
     */
    public function __construct($year = null, $month = null, DateTimeZone $timezone = null)
    {
        $this->setTimezone($timezone);
        $this->setMonth($year, $month);
        $this->setWeekdayDisplayType('full');
        $this->setTemplate($this->getDefaultTemplate());
    }

    /**
     * Sets the month and year for the calendar.
     * @param int $year The 4 digit year.
     * @param int $month The month.
     */
    public function setMonth($year, $month)
    {
        $this->_year    = (!empty($year)) ? $year : date('Y');
        $this->_month   = (!empty($month)) ? $month : date('n');

        // create a Carbon object for the first day of the month
        try
        {
            $this->_monthStartDate = Carbon::createFromDate($this->_year, $this->_month, 1, $this->_timezone);
        }
        catch(Exception $e)
        {
            $this->_year            = date('Y');
            $this->_month           = date('n');
            $this->_monthStartDate  = Carbon::createFromDate($this->_year, $this->_month, 1, $this->_timezone);
        }

        // calculate the last day of the month
        $this->_setEndOfMonth();
    }

    /**
     * Sets the timezone to use for the event dates.
     * @param DateTimeZone $timezone The timezone object.
     */
    public function setTimezone(DateTimeZone $timezone = null)
    {
        $this->_timezone = (!empty($timezone)) ? $timezone : new DateTimeZone('UTC');
    }

    /**
     * Fills the weekdays array with the values based on the specified type.
     * @param string $type The type.
     */
    public function setWeekdayDisplayType($type = 'full')
    {
        switch($type)
        {
            case 'full':
                $weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                break;
            case 'abbreviation':
                $weekdays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
                break;
            case 'short':
                $weekdays = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];
                break;
            default:
                $weekdays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
                break;
        }

        $this->_weekdays = $weekdays;
    }

    /**
     * Sets the calendar template html.
     * @param string $html The html for the calendar template.
     */
    public function setTemplate($html = '')
    {
        $this->_templateHtml = $html;
    }

    /**
     * Sets the limit for events for a single day.
     * @param int $limit The number of events to display.
     */
    public function setDailyEventLimit($limit)
    {
        $this->_dailyEventLimit = (!is_numeric($limit)) ? -1 : $limit;
    }

    /**
     * Sets the use query strings flag.
     * @param boolean $useQueryStrings A flag indicating if query strings should be used.
     */
    public function setUseQueryStrings($useQueryStrings = true)
    {
        $this->_useQueryStrings = $useQueryStrings;
    }

    /**
     * Sets the base url for the month navigation links.
     * @param string $baseUrl The base url.
     */
    public function setMonthBaseUrl($baseUrl)
    {
        $this->_monthNavBaseUrl = $baseUrl;
    }

    /**
     * Sets the base url for the event overflow link.
     * @param string $baseUrl The base url.
     */
    public function setOverflowBaseUrl($baseUrl)
    {
        $this->_overflowBaseUrl = $baseUrl;
    }

    /**
     * Adds an event to the calendar.
     * @param string $title The title of the event.
     * @param Carbon $startDate The start date of the event, expressed as a Carbon object.
     * @param Carbon $endDate The end date of the event, expressed as a Carbon object.
     * @param bool $allDay A flag indicating if the event is an all day event.
     * @param string $url The url to link the event to. Optional.
     */
    public function addEvent($title, Carbon $startDate, Carbon $endDate, $allDay = false, $url = null)
    {
        // convert the dates to the calendar timezone
        $startDate->setTimezone($this->_timezone);
        $endDate->setTimezone($this->_timezone);

        // make sure that the event is for the month & year set for the calendar
        if($startDate->month == $this->_month && $startDate->year == $this->_year)
        {
            $this->_events[$startDate->day][] = [
                                                'title'     => $title,
                                                'startDate' => $startDate,
                                                'endDate'   => $endDate,
                                                'allDay'    => $allDay,
                                                'url'       => $url
                                            ];
        }
    }

    /**
     * Puts together the html for the calendar.
     * @return string $html The html for the calendar display.
     */
    public function render()
    {
        // set up the Twig parser
        $loader = new Twig_Loader_Array(['template' => $this->_templateHtml]);
        $twig   = new Twig_Environment($loader);

        // calculate the current date and the start dates for the next and previous months
        $today          = Carbon::now(null, null, $this->_timezone);
        $nextMonth      = clone $this->_monthStartDate;
        $previousMonth  = clone $this->_monthStartDate;
        $nextMonth->addMonth();
        $previousMonth->subMonth();

        // set up the template variables
        $variables = [
            'thisMonth'             => $this->_monthStartDate,
            'today'                 => $today,
            'useQueryStrings'       => $this->_useQueryStrings,
            'previousMonthUrl'      => ($this->_useQueryStrings) ? $this->_monthNavBaseUrl.'?year='.$previousMonth->year.'&month='.$previousMonth->month : $this->_monthNavBaseUrl.'/'.$previousMonth->year.'/'.$previousMonth->month,
            'nextMonthUrl'          => ($this->_useQueryStrings) ? $this->_monthNavBaseUrl.'?year='.$nextMonth->year.'&month='.$nextMonth->month : $this->_monthNavBaseUrl.'/'.$nextMonth->year.'/'.$nextMonth->month,
            'currentMonthUrl'       => ($this->_useQueryStrings) ? $this->_monthNavBaseUrl.'?year='.$today->year.'&month='.$today->month : $this->_monthNavBaseUrl.'/'.$today->year.'/'.$today->month,
            'eventOverflowBaseUrl'  => $this->_overflowBaseUrl,
            'weekdays'              => $this->_weekdays,
            'weeks'                 => $this->_getWeeks()
        ];

        // parse the template and return it
        $html = $twig->render('template', $variables);
        return $html;
    }

    /**
     * Sets up the default calendar template html.
     * @return string $html The default html for the calendar template.
     */
    public function getDefaultTemplate()
    {
        $html = '
        <h2>{{ thisMonth.format("F Y") }}</h2>
        <ul class="calendar-navigation">
            <li class="previous"><a href="{{ previousMonthUrl }}">Previous</a></li>
            {% if thisMonth.month != today.month or thisMonth.year != today.year %}
                <li class="current"><a href="{{ currentMonthUrl }}">Current</a></li>
            {% endif %}
            <li class="next"><a href="{{ nextMonthUrl }}">Next</a></li>
        </ul>
        <table>
            <thead>
            <tr>
            {% for weekday in weekdays %}
                <td>{{ weekday }}</td>
            {% endfor %}
            </tr>
            </thead>
            <tbody>
            {% for week in weeks %}
                <tr>
                {% for day in week %}
                    <td{% if day.isBlank %} class="blank"{% elseif day.day == today.day and thisMonth.month == today.month and thisMonth.year == today.year %} class="today"{% endif %}>
                    {% if not day.isBlank %}
                        <div class="date">{{ day.day }}</div>
                        <div class="content">
                        {% for event in day.events %}
                            {% if event.allDay %}
                            <div class="event all-day">
                                <span class="time">All Day</span>
                                <span class="title">{{ event.title }}</span>
                            </div>
                            {% else %}
                            <div class="event">
                                <span class="time">{{ event.startDate.format("g:ia") }}</span>
                                <span class="title">{{ event.title }}</span>
                            </div>
                            {% endif %}
                        {% endfor %}
                        {% if day.hasOverflow %}
                            <a href="{{ eventOverflowBaseUrl }}{% if useQueryStrings %}?year={{ thisMonth.year }}&amp;month={{thisMonth.month}}&amp;day={{ day.day }}{% else %}/{{ thisMonth.year }}/{{thisMonth.month}}/{{ day.day }}{% endif %}">View All</a>
                        {% endif %}
                        </div>
                    {% endif %}
                    </td>
                {% endfor %}
                </tr>
            {% endfor %}
            </tbody>
        </table>';

        return $html;
    }

    /**
     * Determine the last day of the month.
     */
    private function _setEndOfMonth()
    {
        // calculate the last day of the month
        $this->_monthEndDate = clone $this->_monthStartDate;
        $this->_monthEndDate->addDay($this->_monthStartDate->daysInMonth-1);
    }

    /**
     * Gets the days of the month grouped into weeks.
     * @return array $weeks An array containing the days grouped by week.
     */
    private function _getWeeks()
    {
        $weeks = [];

        // what day of the week does the first day of the month fall on?
        $firstWeekDayOfWeek = $this->_monthStartDate->dayOfWeek;

        // set the current day to the first day of the month
        $currentDay = clone $this->_monthStartDate;

        // if the first day of the month is not Sunday, pad the week
        if($firstWeekDayOfWeek > 0)
        {
            $firstWeek = [];

            for($i=1; $i<=$firstWeekDayOfWeek; $i++)
            {
                $firstWeek[] = ['day' => null, 'events' => [], 'isBlank' => true, 'hasOverflow' => false];
            }

            // add the days for the first week
            for($i=$firstWeekDayOfWeek; $i<7; $i++)
            {
                // get the events for the day
                $events         = (array_key_exists($currentDay->day, $this->_events)) ? $this->_events[$currentDay->day] : [];
                $hasOverflow    = false;

                // is the number of events bigger than the daily limit?
                if($this->_dailyEventLimit > 0 && count($events) > $this->_dailyEventLimit)
                {
                    $events         = array_slice($events, 0, $this->_dailyEventLimit);
                    $hasOverflow    = true;
                }

                // add the day to the week
                $firstWeek[] = ['day' => $currentDay->day, 'events' => $events, 'isBlank' => false, 'hasOverflow' => $hasOverflow];

                // move to the next day
                $currentDay->addDay(1);
            }

            $weeks[] = $firstWeek;
        }

        // loop through the rest of the month adding the days, grouped in weeks
        while($currentDay->month == $this->_monthStartDate->month)
        {
            $thisWeek = [];
            for($i=0; $i<7; $i++)
            {
                $isOverflow     = ($currentDay->month == $this->_monthStartDate->month) ? false : true;
                $thisDay        = (!$isOverflow) ? $currentDay->day : null;
                $events         = (!$isOverflow) ? ((array_key_exists($currentDay->day, $this->_events)) ? $this->_events[$currentDay->day] : []) : [];
                $hasOverflow    = false;

                // is the number of events bigger than the daily limit?
                if($this->_dailyEventLimit > 0 && count($events) > $this->_dailyEventLimit)
                {
                    $events         = array_slice($events, 0, $this->_dailyEventLimit);
                    $hasOverflow    = true;
                }

                // add the day to the week
                $thisWeek[] = ['day' => $thisDay, 'events' => $events, 'isBlank' => $isOverflow, 'hasOverflow' => $hasOverflow];

                // move to the next day
                $currentDay->addDay(1);
            }

            $weeks[] = $thisWeek;
        }

        return $weeks;
    }
}
