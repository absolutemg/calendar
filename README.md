# Calendar Package

This package is a standalone package that can be used with any framework or project. It renders a monthly gridded calendar with (optional) events.

## Installation

1) To include this package in your application, you must require it in your **composer.json** file. Add the following to the **require** block.

```
"absolutemg/calendar": "dev-master"
```

2) Run the following command to install the package in your application. 

```
composer install
```

If you get warnings about the lock file not being up-to-date, run the following command. It will update your **composer.json** file and install any new packages.

```
composer update --lock
```
